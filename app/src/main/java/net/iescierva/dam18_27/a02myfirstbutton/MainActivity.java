package net.iescierva.dam18_27.a02myfirstbutton;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG="02 My First Button";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //punteros para manejar los objetos
        Button btn = findViewById(R.id.btn1);
        btn.setOnClickListener(this);

    }

    public void onClick(View v){
        Log.d(TAG, "Entrando en OnCreate");
        TextView txt = findViewById(R.id.textView);
        txt.setText("Pulsado!!!");
        Log.d(TAG, "Saliendo de OnCreate");


    }
}
